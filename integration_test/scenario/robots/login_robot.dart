import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

class LoginRobot {
  const LoginRobot(this.driver);

  final FlutterDriver? driver;

  Future<void> enterUsername(String username) async {
    await driver!.tap(find.byValueKey('usernameTextField'));
    await driver!.enterText(username);
  }

  Future<void> enterPassword(String password) async {
    await driver!.tap(find.byValueKey('passwordTextField'));
    await driver!.enterText(password);
  }

  Future<void> tapLoginButton() async {
    await driver!.tap(find.text('Login'));
  }

  Future<void> tapLogoutButton() async {
    await driver!.tap(find.text('Logout'));
  }

// Future<void> tapAlertButton() async {
//   await driver!.tap(find.text('Ok'));
// }

// Future<void> checkInvalidCredentialsMessageIsShown() async {
//   final errorMessageFinder = find.byValueKey('snackbar');
//   await driver!.waitFor(errorMessageFinder);
// }

// Future<void> checkWelcomeMessageIsShown(String username) async {
//   final welcomeMessageFinder = find.text('Welcome $username');
//   await driver!.waitFor(welcomeMessageFinder);
//   expect(await driver!.getText(welcomeMessageFinder), 'Welcome $username');
// }
}

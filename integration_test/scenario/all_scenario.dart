import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

import 'robots/robots.dart';
import 'dart:io';

void main() {
  FlutterDriver? driver;
  late LoginRobot loginRobot;

  setUpAll(() async {
    driver = await FlutterDriver.connect();
    loginRobot = LoginRobot(driver);
  });

  tearDownAll(() async {
    await driver!.close();
  });

  group('Login', () {
    test('1. TextFormField', () async {
      await loginRobot.enterUsername('grow with google');
      await loginRobot.enterPassword('grow with google');
      await driver!.takeScreenshot('tap_input_cc');
      sleep(const Duration(seconds: 1));
    });
  },
      timeout: const Timeout(
        Duration(minutes: 1),
      ));
}

extension on FlutterDriver {
  Future<void> takeScreenshot(String name) async {
    final filePath = File('screenshots/$name.png');
    if (await filePath.exists()) {
      await filePath.delete(recursive: true);
    }
    final file = await filePath.create(recursive: true);
    final png = await screenshot();
    file.writeAsBytesSync(png);
    print('screenshot with name $name was taken');
  }
}
